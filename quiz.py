from cgi import print_directory
import time
import os
from pick import pick
from utils import *

def quiz(WORLD_INITIALIZATION, ANIMATIONS):
    os.system('clear')

    if ANIMATIONS:
        #Introduction du jeu    
        print_slow("   -----   ")
        print_fast("\nBienvenue dans ce quiz géographique\n")
        time.sleep(0.5)
        print_fast("Défie tes amis sur leurs connaissances des pays du monde !")
        print_slow("\n   -----   ")
        time.sleep(2)

    #Durée du jeu
    title = "En combien de manche voulez-vous jouer"
    options = ['2','5','10']
    option, _ = pick(options, title)
    nb_tours = int(option)

    #Initialisation du nombre et de la liste des joueurs
    players = players_initialization()
    time.sleep(0.5)

    os.system('clear')

    if ANIMATIONS:
        data_loading_bar()
        
    #On initialise les positions de départ, les scores 
    #la liste des pays visité et le dernier pays visité de chaque joueur
    position, scores, visited_countries, last_visited_country = game_initialisation(players, WORLD_INITIALIZATION)

    if ANIMATIONS:
        #On annonce les positions des joueurs
        for i in range(len(players)):
            time.sleep(0.5)
            print(f"{players[i]} commence son voyage depuis : ", end="", flush=True)
            time.sleep(1)
            print_slow(f"{position[i]}\n")
            time.sleep(0.7)  

    #On fait jouer chaque joueur sur nb_tours
    for i in range(1,nb_tours + 1):
        
        os.system('clear') 
        print_fast("   -----   \n")
        print_fast(f"Tour {i} sur {nb_tours}\n")
        print_fast("   -----   \n")
        time.sleep(0.5)

        #On fait jouer le joueur n°i
        for player in players:
            print_fast(f"\nAu tour de {player}\n")
            print_fast("----------------------\n")
            time.sleep(1)

            #On récupère l'id du pays actuel
            id_current_country = world_list.loc[world_list['pays'] == last_visited_country[player]].iat[0,0]
            
            # On demande le nom d'un pays frontalier, et on récupère la réponse
            border_country, id_border_country = ask_for_border_country(last_visited_country[player])
            
            #On vérifie si le résultat est correct, et dans le cas positif, on continue avec la question de culture G
            if is_border_country(id_current_country, border_country, id_border_country):
                #On met a jour le dernier pays visité par le joueur
                last_visited_country[player]=border_country
                time.sleep(1)

                #On augmente d'un point le score si le pays n'avait pas été visité
                if border_country not in visited_countries[player]:
        
                    visited_countries[player].add(border_country)
                    scores[player]+=1 

                    print_fast("\nMaintenant, petite question de culture générale sur votre pays d'arrivée.\n")
                    time.sleep(1)

                    #On pose une question de culuture générale sur le nouveau pays
                    if general_knowledge(id_border_country, ANIMATIONS):
                        scores[player]+=1
                else : 
                    print_fast("Vous avez déjà visité ce pays, pas de point supplémentaire pour ce tour.\n")
                
                time.sleep(1)
        
        #A la fin du tour, on annonce le score
        if i!=nb_tours:
            os.system('clear')
            time.sleep(1)
            print_fast("   -----   \n")
            print_fast(f"Annonce des scores du tour {i}\n")
            print_fast("   -----   \n")
            print_slow('\n'.join([f'{player} : {scores[player]}' for player in players]))
            time.sleep(3)
    
    #A la fin du jeu, on annonce le score final
    os.system('clear')
    print_fast("   -----   \n")
    print_fast(f"Annonce des scores finaux\n")
    print_fast("   -----   \n")
    time.sleep(1)
    print_slow('\n'.join([f'{player} : {scores[player]}' for player in players]))
    print("\n   -----   \n")
    best_player = [key for key, value in scores.items() if value == max(scores.values())]
    time.sleep(2)
    print_slow("Annonce du gagnant\n")
    time.sleep(2)
    #On gère les cas d'égalité
    if len(best_player) == 1:
        print_fast(f"{best_player[0]}, félicitation, tu remportes la partie !!! \n")
    else:
        print_fast(f"{', '.join(best_player)}, vous êtes arrivés ex-aequo et vous remportez la partie, bravo à vous !\n")

    #Si on est un peu sadique...
    time.sleep(2)
    print_slow("\nAnnonce du perdant\n")
    time.sleep(1)
    worst_players = set([key for key, value in scores.items() if value == min(scores.values())]) - set(best_player)
    
    #Cas ou pas de perdant car que des gagnant ex-aequo
    if len(worst_players) == 0:
        print_fast("Pas de perdant, vous êtes tous des génis de la géographie !\n")
        time.sleep(0.5)
        print_fast("Ou tous très mauvais... \n")
    if len(worst_players) == 1:
        print_fast(f"{worst_players.pop()}, il s'agirait de s'acheter une carte du monde ou un globe terrestre... \n")
    else:
        print_fast(f"{', '.join(list(worst_players))}, il s'agirait de vous acheter une carte du monde ou un globe terrestre... \n")
    time.sleep(2)

    title = "Voulez-vous continuer ?"
    options = ['Oui', 'Non']
    option, _ = pick(options, title)
    if apply_unicode(option) == 'oui':
        quiz()
    else :
        os.system('clear')
        time.sleep(1)
        print_fast("Merci beaucoup pour votre participation, nous espérons que vous avez passé un très bon moment\n")
        time.sleep(1)
        print_fast("PS : Je pense que cela vaut au moins un 20 vous ne pensez pas ...?\n")
        time.sleep(5)



