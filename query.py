from locale import format_string
from random import randint
import sys
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON

query_all_country_w_capital = """SELECT DISTINCT ?country ?countryLabel ?capital ?capitalLabel
WHERE
{
  ?country wdt:P31 wd:Q3624078 .
  ?country wdt:P36 ?capital .
  FILTER NOT EXISTS {?country wdt:P31 wd:Q3024240}
  FILTER NOT EXISTS {?country wdt:P31 wd:Q28171280}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr" }
}
ORDER BY ?countryLabel"""

def get_results(query : str, endpoint_url = "https://query.wikidata.org/sparql") -> dict :
    """Reçoit une query en entrée qui est run sur Wikidata Query Service.\n
    Renvoie la réponse de cette requête sous un format JSON"""

    user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
    # TODO adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()

def get_neighbours_infos(id_country : str) -> pd.DataFrame:
  """Renvoie un dataframe sous le format pd.DataFrame(pays, columns=['id', 'pays', 'capitale']),
  contenant les informations des pays frontaliers et leur capitale"""

  query_neighbours_capital = f"""SELECT DISTINCT ?country ?countryLabel ?capital ?capitalLabel
  WHERE
  {{
    ?country p:P31 [ps:P31 wd:Q3624078] ;
            wdt:P47 wd:{id_country};
            wdt:P36 ?capital .
    FILTER NOT EXISTS {{?country wdt:P31 wd:Q3024240}}
    FILTER NOT EXISTS {{?country wdt:P31 wd:Q28171280}}
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}
  ORDER BY ?countryLabel"""

  pays, results = [], get_results(query_neighbours_capital)
  for e in results['results']['bindings']:
      pays.append([e['country']['value'].split('/')[-1],e['countryLabel']['value'], e['capitalLabel']['value']])
  return pd.DataFrame(pays, columns=['id', 'pays', 'capitale'])

def get_literal_knowledge_query(country_id : str, index : int) -> str:
  """Renvoie la query correspondant à la question n°[index]"""
  query_general_knowledge_questions = {
    0 : f"""SELECT ?language ?languageLabel
  WHERE
  {{
    wd:{country_id} wdt:P37 ?language . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}
  """,

    1: f"""SELECT DISTINCT ?city ?cityLabel ?population 
    WHERE {{
  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  ?city (wdt:P31/(wdt:P279*)) wd:Q515;
    wdt:P17 wd:{country_id}.
  ?city wdt:P1082 ?population.
  FILTER(?population>200000)
  MINUS {{?city wdt:P1376 wd:{country_id}}}
  }}""",

    2: f"""SELECT ?currency ?currencyLabel
  WHERE
  {{
    wd:{country_id} wdt:P38 ?currency . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}""",

  3: f"""SELECT ?head_of_state ?head_of_stateLabel
  WHERE
  {{
    wd:{country_id} wdt:P35 ?head_of_state . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}""",

  4: f"""SELECT ?driving_side ?driving_sideLabel
  WHERE
  {{
    wd:{country_id} wdt:P1622 ?driving_side . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}""",

  5: f"""SELECT ?capital ?capitalLabel
  WHERE
  {{
    wd:{country_id} wdt:P36 ?capital . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}""",

  6: f"""SELECT ?highp ?highpLabel
  WHERE
  {{
    wd:{country_id} wdt:P610 ?highp . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}""",

  7: f"""SELECT ?hymne ?hymneLabel
  WHERE
  {{
    wd:{country_id} wdt:P85 ?hymne . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}"""
}
  return query_general_knowledge_questions[index]

def get_numerical_knowledge_query(country_id : str, index : int) -> str:
  """Renvoie la query correspondant à la question n°[index]"""
  query_general_knowledge_quesitons = {
    0 : f"""SELECT ?population
  WHERE
  {{
    wd:{country_id} wdt:P1082 ?population . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}"""
}
  return query_general_knowledge_quesitons[index]

def get_neighbours_infos(id_country : str) -> pd.DataFrame:
  """Renvoie un dataframe sous le format pd.DataFrame(pays, columns=['id', 'pays', 'capitale']),
  contenant les informations des pays frontaliers et leur capitale"""

  query_neighbours_capital = f"""SELECT DISTINCT ?country ?countryLabel ?capital ?capitalLabel
  WHERE
  {{
    ?country p:P31 [ps:P31 wd:Q3624078] ;
            wdt:P47 wd:{id_country};
            wdt:P36 ?capital .
    FILTER NOT EXISTS {{?country wdt:P31 wd:Q3024240}}
    FILTER NOT EXISTS {{?country wdt:P31 wd:Q28171280}}
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}
  ORDER BY ?countryLabel"""

  pays, results = [], get_results(query_neighbours_capital)
  for e in results['results']['bindings']:
      pays.append([e['country']['value'].split('/')[-1],e['countryLabel']['value'], e['capitalLabel']['value']])
  return pd.DataFrame(pays, columns=['id', 'pays', 'capitale'])



"""
idées requetes:

* population
SELECT ?population
  WHERE
  {{
    wd:{country_id} wdt:P1082 ?population . 
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr" }}
  }}

"""