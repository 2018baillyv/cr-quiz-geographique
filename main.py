from quiz import *

WORLD_INITIALIZATION = False #Si vrai, départ de n'importe ou dans le monde. Si faux, départ depuis liste prédéfinie
ANIMATIONS = True #Active ou non les animations. Mettre à FALSE pour les tests

def main():
    quiz(WORLD_INITIALIZATION, ANIMATIONS)

if __name__ == "__main__":
    main()
    