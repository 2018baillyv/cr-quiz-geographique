# :earth_americas: -  Quiz géographique

Knowledge scholar project

## :computer: - Équipe 

- BAILLY Valentin
- COURREGES Théo
- GUILLOU-KEREDAN Antoine

## :space_invader: - Sujet

Sujet initial : Jeu dans lequel chaque joueur démarre sur un pays du monde et doit parcourir le plus de pays possible. Pour avancer, il doit à la fois donner le nom d'un pays limitrophe de l'endroit dans lequel il est, et répondre à une question (typiquement trouver la capitale, le nombre d'habitants, une question de culture locale, etc). Les données seront récupérées dans DBpedia, Wikidata ou toute autre base accessible.

## :memo: - Utilisation

Ce dossier est divisé en 5 fichiers différents : 
- Un fichier "main.py" permettant d'excuter le code et de jouer au quiz géographique
- Un fichier "quiz.py" contenant le déroulement du quiz
- Un fichier "utils.py" contenant des fonctions utiles comme l'initialisation de la position des joueurs 
- Un fichier "query.py" contenant la fonction permettant d'effecuter les requêtes sur Wikidata, ainsi que les différentes requêtes sous la forme str.
- Un fichier "old.py" contenant d'anciennes fonctions qui ne sont plus utilisées dans le jeu actuel

Il suffit d'éxecuter le fichier **"main.py"** pour lancer le jeu.  

Le fichier "main.py" contient 2 paramètres booléens:
- "ANIMATIONS" : permet d'activer ou non certaines animations de texte (utile en cas de test pour avancer plus vite)
- "WORLD_INITIALIZATION" : permet de définir la condition de départ. Si True, les joueurs partent de n'importe où dans le monde. Sinon, les joueurs partent d'une liste de pays prédéfinie que l'on retrouve dans "utils.py"

## :floppy_disk: - Modules

Pour faire tourner le jeu, il faudra installer les packages présents dans le requirements.txt et avoir une version de python >= 3.9
