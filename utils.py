from typing import Any
from numpy import iterable
from pick import pick
from unidecode import unidecode
import random
import numpy.random as rd
import time
from query import *

#Liste des pays de départ
pays_depart = ['France', 'Allemagne', 'États-Unis', 'Espagne', 'Portugal', 'République populaire de Chine', 'Maroc', 'Tunisie', 'Afrique du Sud']


#Liste des questions de culture générale
general_knowledge_questions = { 
   'literal' : {
        0 : "Quel est la langue officielle du pays ?",
        1 : "Citez une ville de plus de 200k habitants dans le pays (hors capitale)",
        2 : "Quelle est la monnaie du pays ?",
        3 : "Qui est le/la dirigent(e) du pays ?",
        4 : "De quel côté conduit-on dans ce pays ?",
        5 : "Quelle est la capitale du pays ?",
        6 : "Quel est le point culminant du pays ? (ex: 'Mont Blanc' pour la France)",
        7 : "Quel est le nom de l'hymne national ?"
    },
    'numerical' : {
        0 : "Quelle est la population du pays (en million d'habitants, à 10% près)"
    }

}
def print_fast(str : str) -> str:
    """Simulating human writing, like in games"""
    for letter in str:
        print(letter, end="", flush=True)
        time.sleep(0.035)

def print_very_slow(str : str) -> str:
    """Simulating human writing, like in games"""
    for letter in str:
        print(letter, end="", flush=True)
        time.sleep(0.08)

def print_slow(str : str) -> str:
    """Simulating human writing, like in games"""
    for letter in str:
        print(letter, end="", flush=True)
        time.sleep(0.05)

def apply_unicode(object : str) -> str:
    """Renvoie un string sans accent, sans espace, sans tiret, en lettres minuscules"""
    return unidecode(object.lower().replace('-', '').replace(' ', ''))

def get_world_countries_and_capitals() -> pd.DataFrame:
    """Renvoie la liste des pays du monde entier 
    et leur capitale sous la forme d'un dataframe."""

    pays=[]
    results = get_results(query_all_country_w_capital)
    for e in results['results']['bindings']:
        pays.append([e['country']['value'].split('/')[-1],e['countryLabel']['value'], e['capitalLabel']['value']])
    world_list = pd.DataFrame(pays, columns=['id', 'pays', 'capitale'])
    world_list = world_list.set_index(world_list['id'])
        
    return world_list

#On définit la liste des pays du monde entier et leur capitale qui sera utilisé dans le reste du jeu
world_list = get_world_countries_and_capitals()
check_world_list = world_list.copy()
check_world_list['pays'] = check_world_list['pays'].apply(apply_unicode)
check_world_list['capitale'] = check_world_list['capitale'].apply(apply_unicode)

def process_literal_results(results_of_query : dict) -> list:
    """Prend en entrée un résultat d'une query (question culutre G). 
    \nRenvoie une liste de valeursen lien avec la question 
    \nEx: Liste des villes de + de 2OOk en France -> ['Marseille','Bordeaux',...,'Nantes]."""
    res = []
    for dict in results_of_query['results']['bindings']:
        for element in dict:
            if 'Label' in element:
                res.append(dict[element]['value'])
    return res

def process_numerical_results(results_of_query : dict) -> list:
    """Prend en entrée un résultat d'une query (question culutre G). 
    \nRenvoie la valeur numérique correspondante"""
    res = 0
    dict = list(results_of_query['results']['bindings'].pop().items())[-1][-1]
    
    return float(dict['value'])

def players_initialization() -> tuple[int, list]:
    """Renvoie le nombre de joueur et 
    la liste contenant les noms des joueurs"""
    
    #On demande le nombre de joueurs
    title = "Combien y a-t-il de joueurs ?"
    options = list(map(str,range(2,11)))
    option, _ = pick(options, title)
    number_of_players = int(option)

    #On itialise la liste de joueurs et on demande les pseudos
    players = []
    for i in range(number_of_players):
        print("   -----   ")
        print_fast(f"Joueur {i+1}, quel est votre nom ?\n")
        name = input()
        while name in players:
            print(f"Quelqu'un a déjà ce nom, choisissez un autre pseudo pour vous différencier de {name}\n")
            time.sleep(1)
            print_fast(f"Joueur {i+1}, quel est votre nouvau pseudo ?\n")
            name = input()
        players.append(name)
        
        #On mélange la liste des joueurs pour ne pas forcément commencer dans l'ordre annoncé
        random.shuffle(players)
    
    return players

def init_pos(number_of_player : int, initializer: bool) -> list:
    """Renvoie une liste avec les positions initiales de chaque joueur."""
    list_ = list(world_list['pays']) if initializer == True else pays_depart
    random.shuffle(list_)
    position = [list_.pop() for _ in range(number_of_player)]
    return position

def game_initialisation(players : list(), initializer) -> tuple[list, list, dict, dict]:
    """Initialise le jeu .\n
    Si [initializer] == True, les joueurs partent de n'importe ou dans le monde. 
    Sinon, ils partent d'une liste prédéfinie.\n
    Renvoie la liste des positions initialises (pays), la liste des scores, 
    un dictionnaire des pays visités par chaque joueur et 
    un dictionnaire des derniers pays visités par chaque joueur"""

    position = init_pos(len(players), initializer)
    scores = { players[i] : 0 for i in range(len(players))}
    visited_countries = { players[i] : set(position[i]) for i in range(len(players))}
    last_visited_country = { players[i] : position[i] for i in range(len(players))}

    return position, scores, visited_countries, last_visited_country

def data_loading_bar() -> None:
    """Animation de chargement de données"""
    time.sleep(1)
    print_very_slow("Chargement des données")
    time.sleep(1)
    print(".", end="", flush=True)
    time.sleep(1)
    print(".", end="", flush=True)
    time.sleep(1)
    print(".",end="", flush=True)
    time.sleep(1)
    print_fast("\nDonnées chargées, la partie peut commencer !\n\n")
    time.sleep(1)


def check_answer() -> None:
    """Animation de chargement de données"""
    time.sleep(1)
    print_slow("\nVérification de votre réponse")
    time.sleep(0.3)
    print(".", end="", flush=True)
    time.sleep(0.3)
    print(".", end="", flush=True)
    time.sleep(0.3)
    print(".",end="", flush=True)
    time.sleep(1)

def is_capital(capital : str) -> bool:
    """Vérifie si une ville fournie en entrée est bien une capitale.
    \nRenvoie True si la ville est une capitale."""
    return apply_unicode(capital) in set(world_list['capitale'])

def ask_for_border_country(current_country : str) -> tuple[str, str]:
    """Prend en entrée le pays actuel et demande le nom d'un pays frontalier.\n
    Renvoie les informations du pays frontalier sous la forme [nom, id]"""
    
    print(f"Donnez le nom d'un pays frontalier à votre pays actuel : ", end='', flush=True)
    time.sleep(1)
    print_slow(f"{current_country}\n")
    time.sleep(0.5)
    while True:
        print_slow("Réponse : ")
        border_country = str(input())
        try:   
            id_border_country = check_world_list[check_world_list['pays'] == apply_unicode(border_country)].iat[0,0]
            return world_list.loc[id_border_country,'pays'], id_border_country
        except:
            print(f"{border_country} n'est pas un pays. Rententez votre chance.")

def is_border_country(id_current_country: str, border_country : str, id_border_country: str) -> bool:
    """Renvoie True si le pays entré est bien un pays frontalier."""
    check = apply_unicode(border_country)
    ground_truth = list(get_neighbours_infos(id_current_country)['pays'].apply(apply_unicode))
    
    if check in ground_truth:
        time.sleep(0.5)
        print(f"\nC'est une bonne réponse, vous marquez un point !\n")
        time.sleep(0.5)
        print_fast(f"Vous prenez l'avion et décollez pour : {world_list.loc[id_border_country,'pays']}\n")
        return True
    
    else : 
        time.sleep(0.5)
        print(f"\nMauvaise réponse, la bonne réponse était {(ground_truth[0]).title()}." if len(ground_truth)==1 
                else f"\nMauvaise réponse, les bonnes réponses possibles étaient : \n\n{', '.join(list(ground_truth)).title()}.")
        time.sleep(0.5)
        print_fast("\nRetentez au prochain tour !\n\n")
        time.sleep(1)
        return False

def general_knowledge(id_current_country : str, ANIMATIONS: bool) -> bool:
    """Reçoit en entrée l'id du nouveau pays en entrée et 
    pose une question de culture générale sur ce pays.\n
    Renvoie True si le joueur répond correctement"""

    # On choisit un type de question
    question_type = rd.choice(['literal', 'numerical'], p=[0.80,0.20])
    #On choisit une question au hasard parmi la liste de question définie plus haut
    question_id = rd.randint(0,len(general_knowledge_questions[question_type]))

    #On print la question
    print("   -----   ")
    print(general_knowledge_questions[question_type][question_id])
    print("   -----   ")

    if question_type == 'literal':
        res = literal_question(id_current_country, question_id, ANIMATIONS)
    else:
        res = numerical_question(id_current_country, question_id, ANIMATIONS)
    return res

def numerical_question(id_current_country : str, question_id : int, ANIMATIONS: bool) -> bool:
    """Reçoit en entrée l'id du nouveau pays en entrée et 
    pose une question de culture générale sur ce pays.\n
    Renvoie True si le joueur répond correctement"""

    digit_per_question = {
        0 : {'digit' : 10**6, 'unit' : 'M'}
    }

    ground_truth = process_numerical_results(get_results(get_numerical_knowledge_query(id_current_country, question_id)))
    ground_truth = ground_truth / digit_per_question[question_id]['digit']
    min, max = ground_truth * 0.90, ground_truth * 1.10

    print_slow("Réponse : ")
    answer = float(input())

    if ANIMATIONS:
        check_answer()
        time.sleep(0.5)

    #On vérifie la réponse du joueur
    if min <= answer <= max:
        print(f"\nC'est une bonne réponse, le résultat exact était {round(ground_truth,1)} {digit_per_question[question_id]['unit']}. Vous marquez un point supplémentaire\n")
        return True
    else:
        print(f"\nMauvaise réponse, la bonne réponse était {round(ground_truth,1)} {digit_per_question[question_id]['unit']}.")
        time.sleep(0.5)
        print_fast("\nRetentez au prochain tour ! \n\n")
        time.sleep(1)
        return False

def literal_question(id_current_country : str, question_id : int, ANIMATIONS: bool) -> bool:
    """Reçoit en entrée l'id du nouveau pays en entrée et 
    pose une question de culture générale sur ce pays.\n
    Renvoie True si le joueur répond correctement"""

    #On récupère les réponses possibles à la question
    ground_truth = process_literal_results(get_results(get_literal_knowledge_query(id_current_country, question_id)))
    check_ground_truth = list(map(apply_unicode, ground_truth))
    #On récupère la réponse du joueur
    print_slow("Réponse : ")
    answer = apply_unicode(str(input()))
    
    if ANIMATIONS:
        check_answer()
        time.sleep(0.5)

    #On vérifie la réponse du joueur
    if any([answer in check_ground_truth[i]for i in range(len(ground_truth))]):
        print(f"\nC'est une bonne réponse, vous marquez un point supplémentaire\n")
        return True
    else:
        print(f"\nMauvaise réponse, la bonne réponse était {ground_truth[0].title()}." if len(ground_truth)==1
              else f"\nMauvaise réponse, les bonnes réponses possibles étaient : \n\n{', '.join(ground_truth).title()}.")
        time.sleep(0.5)
        print_fast("\nRetentez au prochain tour ! \n\n")
        time.sleep(1)
        return False



    