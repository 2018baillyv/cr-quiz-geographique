"""Anciennes fonctions pouvant être utile"""


# def ask_for_capital(current_country : str) -> tuple[str,str,str] :
#     """Prend en entrée le pays actuel et demande la capitale d'un pays frontalier.\n
#     Renvoie les informations du pays frontalier sous la forme
#     [border_capital, border_country, id_border_country]"""

#     #Boucle while avec try/except car il y a des bugs quand à cause du stream d'input en cas de mauvaise réponse ou réponse avec
#     #espace, à régler mais fonctionne de manière non optimal avec cette boucle (Antoine)
#     while True:
#         try :
#             print_fast(f"Donnez la capitale d'un pays frontalier à votre pays actuel : {current_country}\n")
#             time.sleep(0.5)
#             print_slow("Réponse : ")
#             border_capital = str(input())
#             apply_unicode(border_capital)

#             #On vérifie que la réponse est bien une capitale
#             while is_capital(border_capital) == False:
#                 time.sleep(0.5)
#                 print(f"{border_capital.capitalize()} n'est pas une capitale ou est mal orthographiée, retentez votre chance !\n")
#                 time.sleep(0.5)
#                 print_fast("Réponse : ")    
#                 border_capital = apply_unicode(str(input()))

#             id_border_country, border_country = (world_list.loc[world_list['capitale'] == border_capital]).iat[0,0], (world_list.loc[world_list['capitale'] == border_capital]).iat[0,1]
#             break
#         except:
#             print_slow("Petit bug de notre part, veuillez retenter...\n")
        
#     return border_capital, border_country, id_border_country

# def is_border_capital(id_current_country : str, border_country : str, boder_capital : str) -> bool:
#     """Renvoie True si la capital appartient à un pays frontalier."""

#     #On récupère les réponses possibles
#     ground_truth = list(get_neighbours_infos(id_current_country)['capitale'].apply(apply_unicode))

#     if apply_unicode(boder_capital) not in ground_truth:
#         time.sleep(0.5)
#         print(f"\nMauvaise réponse, la bonne réponse était {(ground_truth[0]).title()}." if len(ground_truth)==1 
#                 else f"\nMauvaise réponse, les bonnes réponses possibles étaient : \n\n{', '.join(list(ground_truth)).title()}.")
#         time.sleep(0.5)
#         print_fast("\nRetentez au prochain tour ! : \n\n")
#         time.sleep(1)
        
#         return False
#     else:
#         time.sleep(0.5)
#         print(f"\nC'est une bonne réponse, vous marquez un point !\n")
#         time.sleep(0.5)
#         print_fast(f"Vous prenez l'avion et décollez pour : {border_country}\n")
#         return True
